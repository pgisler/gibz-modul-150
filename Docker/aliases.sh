#!/usr/bin/env bash

PLATFORM=Unknown
DOCKER_USER=-u=$(id -u):$(id -g)

if [ "$(uname)" == "Darwin" ]; then
    PLATFORM=MacOSX
    DOCKER_USER=
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    PLATFORM=Linux
    DOCKER_USER=-u=$(id -u):$(id -g)
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    PLATFORM=Windows32
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    PLATFORM=Windows64
fi

# the script is expected to be in [Project Root Folder]/Docker
APPLICATION_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# php/flow aliases
alias php='docker run --rm -it -v=$APPLICATION_PATH:/application -w=/application/${PWD#*$APPLICATION_PATH} \
    -u=php:php --network=${DOCKER_PROJECT_NORMALIZED_NAME}_default gibz-informatik/${DOCKER_PROJECT_NAME}-php-7.1-apache php'
alias composer='docker run --rm -it -v=$APPLICATION_PATH:/application -w=/application/${PWD#*$APPLICATION_PATH} \
    -u=php:php gibz-informatik/${DOCKER_PROJECT_NAME}-php-7.1-apache composer'
alias flow='docker exec -it -u=php:php ${DOCKER_PROJECT_NAME}-server-php71-apache ./flow'