# Shop One50
One50 is a online shop selling fruits, vegetables, herbes and mushrooms to its clients. It is a very simple shop to be used in module 150 at GIBZ.
## Technical Components
### PHP
the application runs on PHP 7.2.10. The minimum required PHP version is 7.2.
### PHP Framework Flow
This project is based on the PHP framework called _Flow_. This is a feature-rich framework for developing powerfull web applications. Please see the online documentation of _Flow_ at [https://flowframework.readthedocs.io/en/stable/index.html](https://flowframework.readthedocs.io/en/stable/index.html). The main application consists of two packages located in the `/Packages/Application` directory.
0. The package `One50.Shop` contains all files which are required for configuring and running the shop.
0. The package `One50.DemoData` contains a collection of dummy products with title, description, random price, images and other attributes. This package is used only for kickstart the shop with some simple products for immediate usage. 
### MySQL Database
The database powering the online shop is a basic MySQL database. Its tables are defined as simple model classes and converted to MySQL database tables using [Doctrine](https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/index.html) as object relationale mapper (ORM).
### Apache webserver
The application comes packed with a preconfigured apache webserver.
### Docker
Docker is used for runnig the whole application with als before mentioned components on any operating system. The only requirement for running the One50 online shop locally is a working and up-to-date installation of Docker. Docker might either run on your host machine (preferred for performance reasons) or in any virtual machine.
## Installation Process
### Prerequisites
Before you start with the installation process as described bellow, please make sure to meet all prerequisites:
#### Command line tool `make`
Depending on your platform, `make` might already be installed. This is usually the case for UNIX based operating systems as Linux or macOS. The recommended way for Windows user is to install Cygwin with `make` tool.
0. Download the installation tool from Cygwin website [https://www.cygwin.com/](https://www.cygwin.com/) (either 64- or 32-bit, depending on your operating system)
0. Run the downloaded installation tool (`setup_x86_64.exe` or `setup_x86.exe`)
0. Choose option `Install from Internet`
0. You may leave the `Root Directory` as proposed
0. Choose a `Local Package Directory` where the downloaded files for the installation process will be stored (you might choose a directory in your Downloads directory)
0. Check `Use System Proxy Settings` as option for the internet connection
0. Choose any available download source from the list of available sites
0. On the page `Select Packages`
	- switch to View option `Full`
	- type `make` in the search field
	- look for the package with description `make: The GNU version of the 'make' utility`
	- click multiply times on the value in column `New` to choose the latest version
	- finish the installation process
0. Run the newly installed cygwin terminal and type the command `make --v` to verify the successful installation
 

### Installation
Follow these steps to install and run the One50 shop locally:
0. Fork project: Fork this git project for your own usage
0. Checkout Fork: Get a local copy of your fork by checking out the remote repository on your local machine

