# Project
export DOCKER_PROJECT_NAME := one50
export DOCKER_PROJECT_NORMALIZED_NAME := one50

export USER_ID := $(shell id -u)
export GROUP_ID := $(shell id -g)

# PHP / Apache
export DEBUG_PORT := 9000

# get host ip (used for xdebug forwarding and phpmyadmin connection)
#ifeq ($(shell uname),Darwin)
#	export DOCKER_HOST := 192.168.65.3
#else
#	export DOCKER_HOST := $(shell ifconfig docker0 | sed -En 's/127.0.0.1//;s/.*inet (addr:|Adresse:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
#endif

export LOCAL_DOMAIN := $(shell basename $$PWD)

APPLICATION_PORT_FILE := Docker/application-port.txt
export APPLICATION_PORT := $(shell [[ -f $(APPLICATION_PORT_FILE) ]] && cat $(APPLICATION_PORT_FILE) || echo 80)




# Database
export MYSQL_DB_NAME := $(DOCKER_PROJECT_NORMALIZED_NAME)
export MYSQL_DB_USER := one50
export MYSQL_DB_PASSWORD := modul150!

DATABASE_PORT_FILE := Docker/database-port.txt
export DATABASE_PORT := $(shell [[ -f $(DATABASE_PORT_FILE) ]] && cat $(DATABASE_PORT_FILE) || echo 3306)



WEB_SERVER_CONTAINER := $(DOCKER_PROJECT_NAME)-server-php71-apache
MYSQL_CONTAINER := $(DOCKER_PROJECT_NAME)-server-mysql
DOCKER_COMPOSE := docker-compose --log-level WARNING --project-name $(DOCKER_PROJECT_NAME) -f Docker/docker-compose.yaml
COMPOSER := docker exec -it -u=php:php $(WEB_SERVER_CONTAINER) composer
MYSQL := docker exec -t -e MYSQL_PWD=modul150! $(MYSQL_CONTAINER) mysql --user=root
FLOW := docker exec -it -u=php:php $(WEB_SERVER_CONTAINER) ./flow


init-server: build-server create-db init-data

build-server: update-docker-compose remove-server remove-cache build-server-pull start-server
	$(COMPOSER) install

# helper script to install/update docker-compose
# see https://github.com/docker/compose/releases for latest version
update-docker-compose:
	if [[ $$(which docker-compose) == "" ]] || [[ $$(docker-compose -v) != *" 1.22."* ]]; then \
	    echo "Installing docker-compose 1.22.0"; \
		sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$$(uname -s)-$$(uname -m) -o /usr/local/bin/docker-compose; \
		sudo chmod +x /usr/local/bin/docker-compose; \
	fi

# remove all containers and volumes for the project
remove-server:
	$(DOCKER_COMPOSE) down
	# remove mysql volume
	docker volume rm  -f $(DOCKER_PROJECT_NORMALIZED_NAME)_mysql || true
	# remove dangling volumes
	docker volume prune -f

# removes files and folders used by flow for cache
remove-cache:
	rm -f Configuration/PackageStates.php
	rm -rf Data

# build the server images, check if there are new base images
build-server-pull:
	$(DOCKER_COMPOSE) build --pull

# start containers
start-server:
	$(DOCKER_COMPOSE) up -d
	if [ "$$(uname)" != "Darwin" ]; then \
		make set-domain-name set-mysql-domain-name; \
	fi

set-domain-name: add-acl-to-hosts-file
	# remove the old entry in the hosts file
	cp -r /etc/hosts /etc/hosts.tmp; \
	sed "/ $(LOCAL_DOMAIN)/d" /etc/hosts.tmp > /etc/hosts; \
	# get local domain based from directory name, get container ip and add to the hosts file \
	WEB_CONTAINER_IP=$$(docker inspect $(WEB_SERVER_CONTAINER) | grep IPAddress | cut -d '"' -f 4 | tr -d '[:space:]'); \
	echo "$$WEB_CONTAINER_IP $(LOCAL_DOMAIN)" >> /etc/hosts

set-mysql-domain-name: add-acl-to-hosts-file
	# remove the old entry in the hosts file
	cp -r /etc/hosts /etc/hosts.tmp; \
	LOCAL_MYSQL_DOMAIN=mysql.$(LOCAL_DOMAIN); sed "/ $$LOCAL_MYSQL_DOMAIN/d" /etc/hosts.tmp > /etc/hosts; \
	# get local domain based from directory name, get container ip and add to the hosts file \
	MYSQL_CONTAINER_IP=$$(docker inspect $(MYSQL_CONTAINER) | grep IPAddress | cut -d '"' -f 4 | tr -d '[:space:]');\
	echo "$$MYSQL_CONTAINER_IP $$LOCAL_MYSQL_DOMAIN" >> /etc/hosts

# add write rights to /etc/hosts for running user to avoid requiring password at every restart
add-acl-to-hosts-file:
	if ! getfacl -p /etc/hosts | grep -q "user:$$(id -u -n):rw"; then \
	     sudo setfacl -m u:$$(id -u -n):rw /etc/hosts; \
	     sudo touch /etc/hosts.tmp; \
	     sudo setfacl -m u:$$(id -u -n):rw /etc/hosts.tmp; \
	     sudo rm /etc/hostsbak; \
	fi

# creates the db schema in one step getting data from the flow models
create-db: test-db-connection
	$(FLOW) doctrine:create

# try to connect the db until it succeeds (sometime the connection is slow to get up after the container starts)
test-db-connection:
	echo -n "waiting for mysql service..."; \
	while !($(MYSQL) -e ";"&>/dev/null); do \
		if [ -z "$$WAITING" ]; then \
            WAITING=TRUE; \
        else echo -n "."; \
        fi; \
	    sleep 1; \
	done; \
	echo " ok"; \

init-data:
	$(FLOW) shopdata:import
	$(FLOW) user:createadmin shopAdmin modul150! ShopAdmin M150

composer-update: composer-update-subpackages
	$(COMPOSER) update

composer-update-subpackages:
	$(COMPOSER) update \
	    one50/shop \
        one50/demodata